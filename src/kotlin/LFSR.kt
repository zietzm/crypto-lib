import kotlin.random.Random

class LFSR {
    val tapPosition: Int
    var lfsr: Int
    private var seedLength: Int = 0

    constructor(seed: String, tapPosition: Int) {
        if (!seed.matches("^[0-1]+$".toRegex())) {
            throw RuntimeException("Seed contains invalid characters")
        }
        if (tapPosition < 0 || tapPosition >= seed.length) {
            throw RuntimeException("Passed tapPosition is out of bounds.")
        }
        this.tapPosition = tapPosition
        this.seedLength = seed.length
        // Convert string seed to Int
        var seedInt: Int = 0
        for (i in 0 until seed.length) {
            var seedValueAtIndex: Int = seed.get(i).toString().toInt()
            seedInt += seedValueAtIndex * (1 shl (seed.length - i - 1))
        }
        this.lfsr = seedInt
    }

    constructor(seedLength: Int, tapPosition: Int) {
        if (seedLength < 0) {
            throw RuntimeException("seedLength must be positive")
        }
        if (tapPosition < 0 || tapPosition >= seedLength) {
            throw RuntimeException("Passed tapPosition is out of bounds.")
        }
        this.tapPosition = tapPosition
        this.seedLength = seedLength
        // Generate random seed
        var randomSeed: Int = 0
        for (i in 0 until seedLength) {
            randomSeed += Random.nextInt(0, 2) * (1 shl (seedLength - i - 1))
        }
        this.lfsr = randomSeed
    }

    override fun toString(): String {
        var returnString: String = ""
        var intValue: Int = lfsr
        for (i in 0 until seedLength) {
            var leastSignificant: Int = intValue % 2
            intValue = intValue / 2
            returnString = leastSignificant.toString() + returnString
        }
        return returnString
    }

    fun nextBit(): Int {
        val bits: Int = (lfsr shr tapPosition) xor (lfsr shr seedLength - 1) and 1
        lfsr = bits or (lfsr shl 1)
        return bits
    }
}
