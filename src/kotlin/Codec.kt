class Codec {
    companion object {
        fun encode(str: String): IntArray {
            var encoded: IntArray = IntArray(7 * str.length)
            for (i in 0 until str.length) {
                var bits: Int = str.get(i).toInt()
                if (bits > 127) {
                    throw RuntimeException("Invalid character '${str.get(i).toString()}'.")
                }
                for (j in 0 until 7) {
                    encoded[7 * i + 6 - j] = bits % 2
                    bits = bits / 2
                }
            }
            return encoded
        }

        fun decode(bits: IntArray): String {
            if (bits.size % 7 != 0) {
                throw RuntimeException("Passed a bitstream that was not a multiple of 7.")
            }
            var returnString: String = ""
            for (i in 0 until bits.size / 7) {
                var charAsInt: Int = 0
                for (j in 0 until 7) {
                    val valueAtIndex: Int = bits[7 * i + j]
                    if (valueAtIndex !in 0..2) {
                        throw RuntimeException("Bits invalid $valueAtIndex")
                    }
                    charAsInt += valueAtIndex * (1 shl (6 - j))
                }
                returnString += charAsInt.toChar().toString()
            }
            return returnString
        }

        fun encrypt(message: IntArray?, seed: String, tapPosition: Int) {
            if (message == null) {
                return
            }
            if (message.size % 7 != 0) {
                throw RuntimeException("Passed a bitstream that was not a multiple of 7.")
            }
            var lfsr: LFSR = LFSR(seed, tapPosition)
            for (i in 0 until message.size) {
                if (message[i] !in 0..2) {
                    throw RuntimeException("Bits invalid ${message[i]}")
                }
                message[i] = lfsr.nextBit() xor message[i]
            }
        }

        fun decrypt(cipher: IntArray, seed: String, tapPosition: Int) {
            encrypt(cipher, seed, tapPosition)
        }
    }
}
