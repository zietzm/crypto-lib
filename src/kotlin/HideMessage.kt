import java.io.File

fun embedMessage(message: IntArray, image: Array<IntArray>) {
    // Embed message into the image in place
    var rowPointer: Int = 0
    var colPointer: Int = 0
    for (bit in message.iterator()) {
        var byte: Int = image[rowPointer][colPointer]
        var updatedByte: Int = byte xor ((-bit xor byte) and 1)
        image[rowPointer][colPointer] = updatedByte
        colPointer += 1
        if (colPointer == image[rowPointer].size) {
            colPointer = 0
            rowPointer += 1
        }
    }
}

fun main(args: Array<String>) {
    val imageFileName: String = args[0]
    var imageArray: Array<IntArray> = ImageData.load(imageFileName)
    val numRows: Int = imageArray.size
    val numCols: Int = imageArray[0].size
    println(numRows * numCols)
    if (numRows == 0 || numCols == 0) {
        throw RuntimeException("$imageFileName is not a valid image file")
    }

    // Load message and append a null terminator
    val messageFileName: String = args[1]
    var message: String = File(messageFileName).readText()
    message += 0.toChar()

    // Encode the string into binary
    var encoded: IntArray = Codec.encode(message)
    if (encoded.size > numRows * numCols) {
        throw RuntimeException("Length of message in $messageFileName exceeded "
                               + "the number pixels in $imageFileName by "
                               + "${encoded.size - (numRows * numCols)} bits")
    }

    if (args.size >= 4) {
        // message must be encrypted
        val seed: String = args[2]
        val tapPosition: Int = args[3].toInt()
        Codec.encrypt(encoded, seed, tapPosition)
    }
    embedMessage(encoded, imageArray)

    if (args.size == 5) {
        // new filename is passed, save image there. Otherwise show it in a window
        val embeddedFileName: String = args[4]
        ImageData.save(imageArray, embeddedFileName)
    } else {
        ImageData.show(imageArray)
    }
}