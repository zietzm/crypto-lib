fun extractFinalBit(imageArray: Array<IntArray>): IntArray {
    val numCols: Int = imageArray[0].size
    val numRows: Int = imageArray.size
    val numBits: Int =  7 * ((numRows * numCols) / 7)
    var outputBits: IntArray = IntArray(numBits)

    for (row in 0 until numRows) {
        val thisRow = imageArray[row]
        for (col in 0 until numCols) {
            if (numCols * row + col >= numBits) {
                break
            } else {
                outputBits[numCols * row + col] = thisRow[col] and 1
            }
        }
    }
    return outputBits
}

fun main(args: Array<String>) {
    val fileName: String = args[0]

    var imageArray: Array<IntArray> = ImageData.load(fileName)
    var embeddedMessage: IntArray = extractFinalBit(imageArray)

    if (args.size == 3) {
        // Message must be decrypted
        val seed: String = args[1]
        val tapPosition: Int = args[2].toInt()
        Codec.decrypt(embeddedMessage, seed, tapPosition)
    }
    var decodedMessage: String = Codec.decode(embeddedMessage)
    println(decodedMessage.substringBefore(0.toChar()))
}